# ML2 Timeseries

Space to work on Time Series Analysis using avocado price data

## Previous installation requirements (using conda env)

A Conda environment is recommended because it will handle all of those in one go. The following steps assume running inside a conda environment.

**prophet and pytorch**

```
conda install -c conda-forge -c pytorch pip fbprophet pytorch cpuonly
```
**darts**

```
pip install u8darts
```

**plotly**
```
conda install -c anaconda plotly 
```
